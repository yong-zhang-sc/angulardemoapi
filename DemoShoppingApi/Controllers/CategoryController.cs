﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DemoShoppingApi.Models;

namespace DemoShoppingApi.Controllers
{
    [RoutePrefix("category")]
    public class CategoryController : ApiController
    {
        private string[] names = { "Loafers", "oxfords", "brogues", "Steve madden", "Jessica Simpson", "Nine West", "Bad Gley", "Ted Baker", "Vince Camuto", "Adrianna Papell" };

        [HttpGet, Route("{cid}")]
        public IHttpActionResult Get(int cid)
        {
            Random r = new Random();
            var data = new List<ItemDto>();

            for (int i = 0; i < 10; i++)
            {
                data.Add(new ItemDto
                {
                    Id = cid * 100 + i,
                    Name = this.names[i],
                    Price = r.Next(1, 200),
                    Image = cid.ToString() + ".jpg"
                });

            }

            return Json(data);
        }
    }
}
