﻿using DemoShoppingApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DemoShoppingApi.Controllers
{
    [RoutePrefix("login")]
    public class LoginController : ApiController
    {
        [HttpPost, Route("")]
        public IHttpActionResult Login([FromBody]Credential credential)
        {
            return Ok();
        }

        [HttpGet, Route("")]
        public IHttpActionResult Get()
        {
            return Json(DateTime.Now);
        }
    }
}
