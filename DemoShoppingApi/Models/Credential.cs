﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoShoppingApi.Models
{
    public class Credential
    {
        public string email { get; set; }

        public string password { get; set; }
    }
}