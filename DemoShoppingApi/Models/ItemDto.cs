﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoShoppingApi.Models
{
    public class ItemDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }

        public string Image { get; set; }
    }
}